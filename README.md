# WORKING WITH DYNAMIC ROUTES (EIGRP)


> This lab is based on the fallowing videos.  
[Jeremy IT's Lab](https://www.youtube.com/watch?v=N8PiZDld6Zc&list=PLxbwE86jKRgMpuZuLBivzlM8s2Dk5lXBQ&index=46)  
[CertBros](https://www.youtube.com/watch?v=QyymlFWDEgM)  


> And the lab itself was created on EVE-NG.  
But should be easily reproduced on Packet Tracer.    
[How to install eve-ng](https://www.eve-ng.net/images/EVE-COOK-BOOK-1.0.pdf)



![topology](./topology.png)

***

***


## CONFIG IPs

**_After you finish configuring ips on each router. Run the command bellow from exec mode to verify that everyting is okay._**  

```  
show ip interface brief
```  

![sh ip int br](./sh-ip-int-br.png)


**_Also while you're configuring a router. Try to ping its neighbors!._**

![ping-neighbors](./ping-neighbors.png)


*** 

## R9

_GET TO CONFIG MODE_  
```
enable  
configure terminal  
hostname R9  
``` 

_CONFIG IPs ON EACH INTERFACE_  
``` 
interface e0/1  
ip address 172.16.128.1 255.255.192.0  
description "connected to Net3"  
no shutdown    


interface e0/2  
ip address 10.0.60.1 255.255.255.252  
description "connected to R10"  
no shutdown  


interface e0/3  
ip address 10.0.40.2 255.255.255.252  
description "connected to R11"  
no shutdown  

```

_RETURN TO USER EXEC MODE_  
``` 
end  
disable  
```

***


### R10

_GET TO CONFIG MODE_  
```  
enable  
configure terminal  
hostname R10  
```

_CONFIG IPs ON EACH INTERFACE_  
```
interface e0/1  
ip address 172.16.192.1 255.255.192.0  
description "connected to net4"  
no shutdown  


interface e0/2  
ip address 10.0.60.2 255.255.255.252  
description "connected to R9"  
no shutdown  


interface e0/3  
ip address 10.0.30.2 255.255.255.252  
description "connected to R12"  
no shutdown  
```  

_RETURN TO USER EXEC MODE_  
```  
end  
disable  
``` 


***


### R11

_GET TO CONFIG MODE_  
```
enable   
configure terminal  
hostname R11  
```  

_CONFIG IPs ON EACH INTERFACE_  
``` 
interface e0/1  
ip address 172.16.0.1 255.255.192.0  
description "connected to Net 1"  
no shutdown  


interface e0/0  
ip address 10.0.40.1 255.255.255.252  
description "connected to R9"  
no shutdown  


interface e0/2  
ip address 10.0.50.1 255.255.255.252  
description "connected to R12"  
no shutdown  


interface e0/3  
ip address 192.168.100.255.255.255.0  
description "connected to WAN"  
no shutdown  

```  

_SET DEFAULT GATEWAY_  
``` 
exit
ip route 0.0.0.0 0.0.0.0 192.168.100.1
``` 

_RETURN TO USER EXEC MODE_  
```
end  
disable  
```  


***


## R12

_GET TO CONFIG MODE_  
``` 
enable  
configure terminal  
hostname R12  
``` 

_CONFIG IPs ON EACH INTERFACE_  
``` 
interface e0/1  
ip address 172.16.64.1 255.255.192.0  
description "connected to Net2"  
no shutdown  


interface  e0/0  
ip address 10.0.30.1 255.255.255.252  
description "connected to R10"  
no shutdown  


interface e0/2  
ip address 10.0.50.2 255.255.255.252  
description "connected to R11"  
no shutdown  
```  

_RETURN TO USER EXEC MODE_    
``` 
end     
disable    
``` 

***

***
## CONFIGURING ROUTES  


**_As extra experience try to run the command bellow before and after the EIGRP config._**

```  
show ip protocol
```

![show ip protocol](./sh-ip-protocol.png)


**_Also you may try this command before setting any EIGRP on all routers._**

![show ip route](./show-ip-route.png)


***

### R9

_GET TO CONFIG MODE_  
``` 
enable  
configure terminal  
```

_SET DYNAMIC ROUTING PROTOCOL AND CONFIGURE NETWORKS_  
``` 
router eigrp 1234  
no auto-summary  
passive-interface e0/1  
network 172.16.128.0 0.0.63.255  
network 10.0.40.0 0.0.0.3  
network 10.0.60.0 0.0.0.3  
```

_RETURN TO USER EXEC MODE_ 
```
end  
disable  
```


**_If you run the command show ip protocol after configuring EIGRP. You'd get this._**  


![sh-ip-prot-pipe-b-eigrp](./sh-ip-prot-pipe-b-eigrp.png)

> We can see in the first line that the Dynamic routing protocol is EIGRP.


> 1234 is the AS(Autonomous System) Number.   
All routers that belong to the same AS must have the same AS Number.  


> Bellow that we have the Metric. Here we are only using de defaults.  
K1 and K3 which represent Bandwidth and Delay value.


> Then we have the Router-ID. The (RID) single purpose is to identify the router.  
If the router have loopback interfaces configured. The loobpack interface with the highest value will be used as the RID.    
Else it will look for the highest values among the physical interfaces.  
Also we can set this value manually. If we do that the value set by hand will have priority over everything else.


> Then we have Distance: Internal 90.  
This is the default Administrative Distance value set for EIGRP.


> Automatic Summarization: disabled was set when we ran no auto-summary.  
We did because we don't want the router advartizing old A,B,C classic subnets by default.  
We want it to be able to advertize subnettigs such as 10.0.40.0/30 or 172.16.128.0/18  


> The ID value is used by to decided which protocol shold be used and we have routers supporting a variary of protocols in the same AS.

> Routing for Networks: Simply put means that this networks will be advertized to any 
routers that R9 has developed "neighborships". 

> Passive Interface is an interface that the R9 wont send "hello messages".  
Or in oder words, interfaces that the router  will not use to try new "neighborships".  
However the networks attached to this interfaces will still be advartized for any router downstream. 



***


### R10

_GET TO CONFIG MODE_
``` 
enable  
configure terminal  
```

_SET DYNAMIC ROUTING PROTOCOL AND CONFIGURE NETWORKS_  
```  
router eigrp 1234  
no auto-summary   
passive-interface e0/1  
network 172.16.192.0 0.0.63.255  
network 10.0.60.0 0.0.0.3  
network 10.0.30.0 0.0.0.3  
```  

_RETURN TO USER EXEC MODE_  
```
end  
disable   
```

**_During configuration of R10 you should receive a message like this._**

![new adjacency](./new-adjacency.png)

> Because you already configured R9. And specialy because R9 and R10 are in the same Autonomous System.  
They will be able to share routes between each other.



**_Now if you run show ip route after configuring R10. We'll see that it already has
routes to Net3 and for R11_**

![show ip route eigrp](./show-ip-router-eigrp.png)

**_So we should expect to be able to ping R11 on interface e0/0 (10.0.40.1). Right ?_**

![ping failed](./ping_failed.png)

**_Actually no!!! Remember we have a route to R11. but R11 doesn't now how to reach R10 yet!!_**


***


### R11

_GET TO CONFIG MODE_  
``` 
enable   
configure terminal   
```

_SET DYNAMIC ROUTING PROTOCOL AND CONFIGURE NETWORKS_  
```
router eigrp 1234   
no auto-summary  
passive-interface e0/1  
network 172.16.0.0 0.0.63.255  
network 10.0.50.0 0.0.0.3   
network 10.0.40.0 0.0.0.3  
network default-information originate  
```  

_RETURN TO USER EXEC MODE_  
```
end  
disable  
```

**_Now as you can see ping between R10 and R11 works!!_**

![ping successfull](./ping-succesful.png)


***   

### R12

_GET TO CONFIG MODE_  
```
enable  
configure terminal  
```

_SET DYNAMIC ROUTING PROTOCOL AND CONFIGURE NETWORKS_  
```
router eigrp 1234  
no auto-summary  
passive-interface e0/1  
network 172.16.64.0 0.0.63.255  
network 10.0.30.0 0.0.0.3  
network 10.0.50.0 0.0.0.3  
```

_RETURN TO USER EXEC MODE_  
```  
end  
disable  
```


**_Now if you run the command bellow. You'll be able to see all routes the were added using EIGRP._**  

```
show ip route eigrp
```

![show eigrp](./final-show.png)

***

Now it's time to play with it. And if everything is working fine don't forget to save (execute this on all routers)!!!

From priv-exec-mode run:

```
write
```

**Now that you got here. You should have easy time configuring VPCs (Virtual PC). So I won´t mention here** 

#### That's all !! Thanks for using this tutorial!!




*** 
